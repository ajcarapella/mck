<div class="team-loop">

	<div class="team-loop__section">
		<h2>Principals</h2>

		<?php $loop = new WP_Query( array( 'post_type' => 'team', 'posts_per_page' => -1, 'meta_key' => 'category', 'meta_value'	=> 'principal', 'order' => 'ASC' )); ?>

			<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
			        <div class="two-col">
				        <img class="headshot" src="<?php echo the_post_thumbnail_url();?>"/>
				        <strong><?php echo get_the_title(); ?>,&nbsp;</strong>
						<?php echo(the_content()); ?>
			        </div>
			<?php endwhile; wp_reset_query(); ?>

			<p class="note">Bob and Ted actively managed MCK until 2013 when company ownership began shifting to the current Principals. Both remain involved in the company, particularly in the areas of development and finance.</p>

	</div>



	<div class="team-loop__section">
		<h2>Company Founders</h2>

		<?php $loop = new WP_Query( array( 'post_type' => 'team', 'posts_per_page' => -1, 'meta_key' => 'category', 'meta_value'	=> 'founder', 'order' => 'ASC' )); ?>
			<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
			        <div class="two-col">
				        <img class="headshot" src="<?php echo the_post_thumbnail_url();?>"/>
			        	<strong><?php echo get_the_title(); ?>,&nbsp;</strong>
						<?php echo(the_content()); ?>
			        </div>
			<?php endwhile; wp_reset_query(); ?>

			<p class="note">Bob and Ted actively managed MCK until 2013 when company ownership began shifting to the current Principals. Both remain involved in the company, particularly in the areas of development and finance.</p>


	</div>



	<div id="management-loop" class="team-loop__section">

		<h2>Management <img class="title-arrow" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAEzSURBVGhD7c8xTsNAFEVRS7SsgS49a0A0CCkrQYJlwZ5Ily1Q0Nhcm3lFIid47E88o7wjjRzb4z+5jZmZmZmZjem67q5t26d0+y+Yv2E9pNt4KWLH+mZt0+NQKWLP+uK8x/Q4jiK4DvgdHsO8ISId0Z8RG8OwgwjhWVgMcw4ihGcxMQwZjRDeLY7h+9EI4d2yGD6+ZcjnMO0M9syO4buzEcKePuY+fZaPAa+s9nfcaWzJjmH/pIge+9653KRP52FIeAz7LhshDAuL4X1OxAeXmAhh6OIYnq8bIQyfHcN9GRHCIdkxXMuKEA7LiXlhlRchHDopZqpVIiQqZtUIWRpTRITMjSkqQnJjioyQqTFFR8hfMVVEyKmYqiLkOKbKCOHPv/UxqDdCiHiuPsLMzMzsqjXND7KTz8ttYE7nAAAAAElFTkSuQmCC" alt="title arrow"/></h2>
		<?php $loop = new WP_Query( array( 'post_type' => 'team', 'posts_per_page' => -1, 'meta_key' => 'category', 'meta_value'	=> 'management', 'order' => 'ASC' )); ?>
			<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
			        <strong><?php echo get_the_title(); ?>,&nbsp;</strong><?php echo(the_content()); ?>
			<?php endwhile; wp_reset_query(); ?>


	</div>

</div>
