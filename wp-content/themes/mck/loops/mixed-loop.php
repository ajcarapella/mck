<div class="project-loop">

	<div class="project-loop__section">
		
		<?php $loop = new WP_Query( array( 'post_type' => 'project', 'posts_per_page' => -1, 'meta_key' => 'project_category', 'meta_value' => 'mixed' )); ?>
			<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
			        <div class="four-col">
				        <img class="property-shot" src="<?php echo the_post_thumbnail_url();?>"/>
				        <span class="property-title"><?php echo get_the_title(); ?></span>
				        <strong><?php echo the_field('project_location');?>&nbsp;-&nbsp;</strong>
						<p><?php echo substr(strip_tags(get_the_content()),0,60).'..'; ?></p>
						<a href="<?php the_permalink();?>">Details &rsaquo;</a>
			        </div>
			<?php endwhile; wp_reset_query(); ?>
	</div>
	
				
</div>