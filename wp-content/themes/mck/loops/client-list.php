<div class="client-list">
	<div class="client-list__row">

		<span class="client-list__title">General Construction <img class="title-arrow" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAEzSURBVGhD7c8xTsNAFEVRS7SsgS49a0A0CCkrQYJlwZ5Ily1Q0Nhcm3lFIid47E88o7wjjRzb4z+5jZmZmZmZjem67q5t26d0+y+Yv2E9pNt4KWLH+mZt0+NQKWLP+uK8x/Q4jiK4DvgdHsO8ISId0Z8RG8OwgwjhWVgMcw4ihGcxMQwZjRDeLY7h+9EI4d2yGD6+ZcjnMO0M9syO4buzEcKePuY+fZaPAa+s9nfcaWzJjmH/pIge+9653KRP52FIeAz7LhshDAuL4X1OxAeXmAhh6OIYnq8bIQyfHcN9GRHCIdkxXMuKEA7LiXlhlRchHDopZqpVIiQqZtUIWRpTRITMjSkqQnJjioyQqTFFR8hfMVVEyKmYqiLkOKbKCOHPv/UxqDdCiHiuPsLMzMzsqjXND7KTz8ttYE7nAAAAAElFTkSuQmCC" alt="title arrow"/></span>

		<div class="client-list__col-two">
			<span class="client-list__subtitle">Accounting</span>
			<ul>
				<li>BMC Accounting</li>
				<li>Bonadio Group</li>
				<li>Firley, Moran Freer & Essa, Accountants</li>
			</ul>

			<span class="client-list__subtitle">Airports & Airlines</span>
			<ul>
				<li>American Airlines</li>
				<li>Hancock International Airport</li>
				<li>U.S. Airways</li>
			</ul>

			<span class="client-list__subtitle">Architecture & Engineering</span>
			<ul>
				<li>King and King, Architects</li>
				<li>Siemens Corporation</li>
			</ul>

			<span class="client-list__subtitle">Banking & Finance</span>
			<ul>
				<li>Binghamton Savings Bank</li>
				<li>Chemical Bank</li>
				<li>Morgan Stanley</li>
				<li>Skaneateles Savings Bank</li>
			</ul>

			<span class="client-list__subtitle">Building Materials & Manufacturing</span>
			<ul>
				<li>Ben Weitsman and Sons</li>
				<li>JGB Enterprises</li>
				<li>NEC Corporation</li>
				<li>NUCOR Steel Corporation</li>
			</ul>


			<span class="client-list__subtitle">Colleges & Universities</span>
			<ul>
				<li>Cornell University</li>
				<li>Cortland State University</li>
				<li>Hobart College</li>
				<li>St. Lawrence University</li>
				<li>Syracuse University</li>
			</ul>


			<span class="client-list__subtitle">Government Agencies</span>
			<ul>
				<li>Auburn Local Development Corporation</li>
				<li>Cicero Local Development Corporation</li>
				<li>National Housing Trust</li>
				<li>Onondaga County</li>
				<li>State of New York</li>
				<li>Syracuse Economic Development Corporation</li>
				<li>Syracuse Housing Authority</li>
				<li>United States Army</li>
				<li>United States Federal Government</li>
				<li>United States Postal Service</li>
			</ul>


			<span class="client-list__subtitle">Insurance</span>
			<ul>
				<li>Mass Mutual Insurance Corporation</li>
				<li>Prudential Insurance Corporation</li>
				<li>Traveller’s Insurance Corporation</li>
			</ul>


			<span class="client-list__subtitle">Law Firms</span>
			<ul>
				<li>Bond, Schoeneck and King, Law Firm</li>
				<li>Costello, Cooney and Fearon, Law Firm</li>
				<li>Devorsetz Law Firm</li>
			</ul>


		</div>






		<div class="client-list__col-two">

			<span class="client-list__subtitle">Leisure</span>
				<ul>
					<li>Cicero Twin Rinks/YMCA Recreation Center</li>
					<li>Skaneateles Country Club</li>
					<li>Skaneateles Festival</li>
					<li>The Regional Market of Syracuse</li>
				</ul>

			<span class="client-list__subtitle">Marketing</span>
				<ul>
					<li>Eric Mower and Associates</li>
				</ul>

			<span class="client-list__subtitle">Personal</span>
				<ul>
					<li>Robert Congel</li>
				</ul>

			<span class="client-list__subtitle">Real Estate</span>
				<ul>
					<li>Partnership Properties</li>
					<li>Pyramid Corporation</li>
					<li>Sutton Companies</li>
					<li>Vornado Corporation</li>
				</ul>


			<span class="client-list__subtitle">Residential & Hotels</span>
				<ul>
					<li>22 Fennell Street, LLC</li>
					<li>Cox Building, LLC</li>
					<li>Finger Lakes Lodging</li>
					<li>Inns of Aurora</li>
					<li>Peregrine Development Assisted Living</li>
				</ul>


			<span class="client-list__subtitle">Restaurants</span>
				<ul>
					<li>Bluewater and Sweetwater Cafe</li>
					<li>Kreb’s Restaurant</li>
					<li>The Dinosaur Bar-b-que</li>
				</ul>

			<span class="client-list__subtitle">Retail</span>
				<ul>
					<li>Sears Corporation</li>
					<li>Steinbach Corporation</li>
				</ul>


			<span class="client-list__subtitle">School Districts</span>
				<ul>
					<li>Baldwinsville Central School District</li>
					<li>Clyde-Savannah</li>
					<li>Ithaca Central School District</li>
					<li>Jordan-Elbridge Central School District</li>
					<li>Lafargeville Central School District</li>
					<li>Lafayette Central School District</li>
					<li>Lansing Central School District</li>
					<li>Liverpool Central School District</li>
					<li>Skaneateles Central School District</li>
					<li>Southern Cayuga Central School District</li>
					<li>Syracuse City School District</li>
					<li>West Genesee Central School District</li>
					<li>Westhill Central School District</li>
				</ul>


			<span class="client-list__subtitle">Telecommunications</span>
				<ul>
					<li>Bell Atlantic</li>
				</ul>


			<span class="client-list__subtitle">Technology & Technology Services</span>
				<ul>
					<li>Rapid Response</li>
					<li>Welch-Allyn Corporation</li>
				</ul>


		</div>


	</div> <!--end row-->


	<div class="client-list__row">

		<span class="client-list__title">Partnership, <br>Development and <br>Design-Build Relationships <img class="title-arrow" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAEzSURBVGhD7c8xTsNAFEVRS7SsgS49a0A0CCkrQYJlwZ5Ily1Q0Nhcm3lFIid47E88o7wjjRzb4z+5jZmZmZmZjem67q5t26d0+y+Yv2E9pNt4KWLH+mZt0+NQKWLP+uK8x/Q4jiK4DvgdHsO8ISId0Z8RG8OwgwjhWVgMcw4ihGcxMQwZjRDeLY7h+9EI4d2yGD6+ZcjnMO0M9syO4buzEcKePuY+fZaPAa+s9nfcaWzJjmH/pIge+9653KRP52FIeAz7LhshDAuL4X1OxAeXmAhh6OIYnq8bIQyfHcN9GRHCIdkxXMuKEA7LiXlhlRchHDopZqpVIiQqZtUIWRpTRITMjSkqQnJjioyQqTFFR8hfMVVEyKmYqiLkOKbKCOHPv/UxqDdCiHiuPsLMzMzsqjXND7KTz8ttYE7nAAAAAElFTkSuQmCC" alt="title arrow"/></span>

		<div class="client-list__col-two">
			<ul>
				<li>Autumntree Court Associates, LLC</li>
				<li>Camillus Mills, LLC</li>
				<li>Franklin Bevier, LLC</li>
				<li>Franklin Properties, LLC</li>
				<li>Lake Architectural</li>
				<li>Lakeview House Associates, LLC</li>
				<li>Logan Building Associates, LLC</li>
				<li>MacKnight Architects</li>
				<li>Marcellus Free Library</li>
				<li>Marcellus Village Woods Associates</li>
				<li>Mirbeau Inn and Spa, LLC</li>
				<li>Patrick Dooley</li>
			</ul>
		</div>


		<div class="client-list__col-two client-list-mobile-fix">
			<ul>
				<li>QPK Architects, Steve Krause</li>
				<li>Ramsgard Architectural</li>
				<li>Robert Neumann</li>
				<li>Seitz Building Associates, LLC</li>
				<li>Stevedore Associates, LLC</li>
				<li>Testone, Marshall & Discenza, Accountants</li>
				<li>Teton Associates, Inc</li>
				<li>Thayer House Associates, LLC</li>
				<li>Ultimate Goal Family Sports Center -- Marcellus</li>
				<li>Ultimate Goal Family Sports Center -- Watertown</li>
				<li>Upper Crown Mill Associates, LLC</li>
				<li>Willowstreet Lofts Associates, LLC</li>
			</ul>
		</div>


	</div> <!--end row-->


</div>
