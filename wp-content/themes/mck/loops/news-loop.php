<?php $loop = new WP_Query( array( 'post_type' => 'post', 'posts_per_page' => -1 )); ?>
<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
       <div class="news-post">
	       <strong><?php echo get_the_title(); ?></strong><br>
		   <p><a class="news-link" href="<?php the_permalink();?>"><?php echo substr(strip_tags(get_the_content()),0,60).'..'; ?></a></p>
       </div>
<?php endwhile; wp_reset_query(); ?>
