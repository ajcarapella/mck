<footer>
	<div class="footer-container">
		<div class="sbe-logo">
			<img src="<?php echo get_template_directory_uri(); ?>/images/sbe.png" alt="Syracuse Builders Exchange"/>
		</div>
		<span>MCK Building Associates, Inc. <div class="divider">|</div> 221 W. Division St., Syracuse, NY 13204</span>
		<br>
		<span>P (315) 475-7499 <div class="divider">|</div> F (315) 471-8020</span>
		<p>© 2018 MCK Building Associates, Inc. <div class="divider">|</div> Powered by: Total Advertising, Inc.</p>
	</div>
</footer>


<?php wp_footer(); ?>
</body>
</html>
