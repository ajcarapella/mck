<?php get_header(); ?>
<?php if(have_posts()): while(have_posts()): the_post(); ?>
<section id="cms-main" class="cms-main-alt">
		<?php if ( has_post_thumbnail() ) { ?>
				<img class="cms-main-background" src="<?php echo the_post_thumbnail_url();?>"/>
			<?php } ?>
			
			
			<div class="cms-content-container ccc-background">
				<span class="single-date"><?php echo get_the_date();?></span>
				<h1><?php echo the_title();?></h1>
				<?php if ( has_post_thumbnail() ) { ?>
					<img class="featured-img" src="<?php echo the_post_thumbnail_url();?>"/>
				<?php } ?>
				
				<div class="single-content"><?php echo(the_content()); ?></div>
				
			</div>


</section>

<?php endwhile; endif;?>
<?php get_footer(); ?>
