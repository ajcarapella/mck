<?php get_header(); ?>
<?php if(get_field('template') == "default"): ?>
<div class="cms-banner">
    <div class="cb-container">
      <img class="cb-background" src="<?php echo the_post_thumbnail_url();?>" />

        <?php if( is_page('35')):?>

  				  <div class="contact-caption">
  				    <h2>MCK Building Associates, Inc.</h2>
  				    <h3>221 West Division Street<br>Syracuse, NY 13204</h3>
              <p>P: (315) 475-7499</p>
              <p>F: (315) 471-8020</p>
  				    <a href="mailto:info@mckbuildingassociates.com">info@mckbuildingassociates.com</a>
  				  </div>

        <?php endif; ?>

    </div>
</div>
<?php endif; ?>

<section id="cms-main" <?php if(get_field('template') == "background"): ?>class="cms-main-alt"<?php endif; ?>>
		<?php if(get_field('template') == "background"): ?>
			<?php if ( has_post_thumbnail() ) { ?>
					<img class="cms-main-background" src="<?php echo the_post_thumbnail_url();?>"/>
			<?php } ?>
		<?php endif; ?>

		<div class="cms-content-container<?php if(get_field('template') == "background"): ?> ccc-background<?php endif; ?>">

			<h1 class="main-headline"><?php the_title();?></h1>

			<?php include_once('loops/flex-items.php');?>


			<!-- News Page-->
			<?php if( is_page('7')):?>
				<?php include_once('loops/news-loop.php');?>
			<?php endif; ?>


			<!-- Team page -->
			<?php if( is_page('11')):?>
				<?php include_once('loops/team-loop.php');?>
			<?php endif; ?>

			<!-- Development page -->
			<?php if( is_page('15')):?>
				<?php include_once('loops/dev-loop.php');?>
			<?php endif; ?>

			<!-- Portfolio Commercial page -->
			<?php if( is_page('19')):?>
				<?php include_once('loops/commercial-loop.php');?>
			<?php endif; ?>

			<!-- Portfolio Public page -->
			<?php if( is_page('21')):?>
				<?php include_once('loops/public-loop.php');?>
			<?php endif; ?>

			<!-- Portfolio Mixed page -->
			<?php if( is_page('23')):?>
				<?php include_once('loops/mixed-loop.php');?>
			<?php endif; ?>

			<!-- Portfolio Hospitality page -->
			<?php if( is_page('25')):?>
				<?php include_once('loops/hospitality-loop.php');?>
			<?php endif; ?>

			<!-- Portfolio Residential page -->
			<?php if( is_page('27')):?>
				<?php include_once('loops/residential-loop.php');?>
			<?php endif; ?>

			<!-- Portfolio Residential page -->
			<?php if( is_page('29')):?>
				<?php include_once('loops/rec-loop.php');?>
			<?php endif; ?>

			<!-- Client List page -->
			<?php if( is_page('31')):?>
				<?php include_once('loops/client-list.php');?>
			<?php endif; ?>

			<!-- Vendors page -->
			<?php if( is_page('33')):?>
				<?php include_once('loops/vendor.php');?>
			<?php endif; ?>

			<!-- Contact page -->
			<?php if( is_page('35')):?>
				<?php include_once('loops/contact.php');?>
			<?php endif; ?>

		</div>
</section>


<?php if( is_page('35')):?>
	<iframe class="contact-map" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2915.173624169707!2d-76.1600222845214!3d43.058815779146194!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89d9f3ca35921a95%3A0xbd08ecd90dc68a2d!2s221+W+Division+St%2C+Syracuse%2C+NY+13204!5e0!3m2!1sen!2sus!4v1540398434519" frameborder="0" style="border:0; width:100%; height:500px;" allowfullscreen></iframe>

<a href="https://www.google.com/maps?ll=43.058816,-76.157834&z=16&t=m&hl=en-US&gl=US&mapclient=embed&daddr=221+W+Division+St+Syracuse,+NY+13204@43.0588158,-76.1578336" target="_blank" class="directions-link">Get Directions »</a>
<?php endif; ?>

<?php get_footer(); ?>
