<?php get_header(); ?>
<?php if(have_posts()): while(have_posts()): the_post(); ?>
<section id="cms-main" class="cms-main-alt">
		<?php if ( has_post_thumbnail() ) { ?>
				<img class="cms-main-background" src="<?php echo the_post_thumbnail_url();?>"/>
			<?php } ?>
			
		<div class="cms-content-container ccc-background">
			<div class="single-project-info">
				<h1 class="single-project-info__h1"><?php echo the_title();?></h1>
				<span class="single-project-info__span"><?php echo the_field('project_location');?></span>
				<p class="single-project-info__p"><?php echo(get_the_excerpt()); ?></p>
		
				<?php if( have_rows('project_gallery') ): ?>
					<div id="projectCarousel" class="carousel slide" data-ride="carousel">
					  <!-- Slides -->	
					  <div class="carousel-inner">
						 <?php $z = 0;
							 	while( have_rows('project_gallery') ): the_row();
							 	$image = get_sub_field('gallery_image');
							 	$alt = get_sub_field('gallery_image_title');?>
						
							    <div class="carousel-item <?php if ($z==0) { echo 'active';} ?>">
										<img class="slide-image" src="<?php echo $image; ?>" alt="<?php echo $alt; ?>" />
							    </div>
						  
								<?php $z++; 
									endwhile; ?>    
				  		</div>
				  		
				  		<!-- Bullets 
				  		  <div class="carousel-indicator-container">
							<ol class="carousel-indicators">
								<?php $i=0;
								while( have_rows('project_gallery') ): the_row();
								if ($i == 0) {
									echo '<li data-target="#projectCarousel" data-slide-to="0" class="active"></li>';
								} else {
									echo '<li data-target="#projectCarousel" data-slide-to="'.$i.'"></li>';
								}
								$i++; endwhile; ?>
							</ol>
						  </div>
						  -->
						  
						  
						  <!-- Thumbnail Navigation -->
						  <div class=" carousel-indicator-container carousel-indicator-container-thumbs">
							  <ol class="carousel-indicators">
								 <?php 
								$i2=0;
								while( have_rows('project_gallery') ): the_row();
								$thumb = get_sub_field('gallery_image');
							 	?>
							 	
									<li data-target="#projectCarousel" data-slide-to="<?php echo $i2;?>" class="<?php if ($i2==0) { echo 'active';} ?>">
										<img class="d-block w-100" src="<?php echo $thumb;?>" class="img-fluid">
									</li>
									
								
								<?php $i2++; endwhile; ?>
								</ol>
						  </div>
						  
				  		
					</div>
					
					 <!-- Left and right controls -->
				  <a class="carousel-control-prev" href="#projectCarousel" data-slide="prev">
					  <i class="fas fa-chevron-left"></i>
				  </a>
				  <a class="carousel-control-next" href="#projectCarousel" data-slide="next">
				  		<i class="fas fa-chevron-right"></i>
				  </a>
					
					
				<?php endif; ?>
		
			</div>
			
		</section>
	
	
<?php endwhile; endif;?>
	
	      
<?php //get_template_part('loops/single-post', get_post_format()); ?>

<?php get_footer(); ?>
