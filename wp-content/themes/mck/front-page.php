<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that other
 * 'pages' on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage custom
 * @since custom 1.0
 */

get_header(); ?>
<!-- Main Banner -->

<section id="carousel-block">
	<div id="homeCarousel" class="carousel slide" data-ride="carousel">
	  <div class="carousel-inner">
	    <div class="carousel-item active">
	      <img class="carousel-bg" src="<?php echo get_template_directory_uri(); ?>/images/550.jpg" alt="First slide">
			  <div class="carousel-caption-container">
				  <div class="carousel-caption d-block home-fade-in">
				    <h2>Bridgewater <br>Place</h2>
				    <h3>Syracuse, NY</h3>

				    <p>Full-scale office building renovation.</p>
				    <a href="#">Details ></a>
				  </div>
			  </div>
	    </div>
	    <div class="carousel-item">
	      <img class="carousel-bg" src="<?php echo get_template_directory_uri(); ?>/images/550.jpg" alt="First slide">
			  <div class="carousel-caption-container">
				  <div class="carousel-caption d-block home-fade-in">
				    <h2>Bridgewater <br>Place 2</h2>
				    <h3>Syracuse, NY</h3>

				    <p>Full-scale office building renovation.</p>
				    <a href="#">Details ></a>
				  </div>
			  </div>
	    </div>
	    <div class="carousel-item">
	      <img class="carousel-bg" src="<?php echo get_template_directory_uri(); ?>/images/550.jpg" alt="First slide">
			  <div class="carousel-caption-container">
				  <div class="carousel-caption d-block home-fade-in">
				    <h2>Bridgewater <br>Place 3</h2>
				    <h3>Syracuse, NY</h3>

				    <p>Full-scale office building renovation.</p>
				    <a href="#">Details ></a>
				  </div>
			  </div>
	    </div>
	  </div>


	  <div class="carousel-indicator-container">
		  <ol class="carousel-indicators">
		  	<li data-target="#homeCarousel" data-slide-to="0" class="active"></li>
		  	<li data-target="#homeCarousel" data-slide-to="1"></li>
		  	<li data-target="#homeCarousel" data-slide-to="2"></li>
		  </ol>
	  </div>
	</div>
</section>

<section id="information-block">
    <div id="ib-container" class="container-pad">
		<h1>The expertise of a builder combined <br>with the vision of a developer.</h1>
		<p>MCK Building Associates, Inc., is a development-oriented construction company with an emphasis on design/build<br>projects. Based in Syracuse, New York, MCK prides itself on the mixture of quality and creativity it builds into each of its<br>jobs. We build where people live, work, and relax. And we strive for excellence in creating these environments.</p>
    </div>
</section>

<section id="teaser-block" class="teaser-block">
	<div class="teaser-block__container" class="container-pad">
		<div class="teaser-block__row">
			<div class="teaser-block__block" style="background:url('<?php echo get_template_directory_uri(); ?>/images/550.jpg');">
				<div class="tbb-label">
				</div>
					<span>Commercial</span>
			</div>
			<div class="teaser-block__block" style="background:url('<?php echo get_template_directory_uri(); ?>/images/550.jpg');">
				<div class="tbb-label">
				</div>
					<span>Public &amp; <br>Institutional</span>
			</div>
		</div>

		<div class="teaser-block__row">
			<div class="teaser-block__block" style="background:url('<?php echo get_template_directory_uri(); ?>/images/550.jpg');">
				<div class="tbb-label">
				</div>
					<span>Mixed-Use</span>

			</div>
			<div class="teaser-block__block" style="background:url('<?php echo get_template_directory_uri(); ?>/images/550.jpg');">
				<div class="tbb-label">
				</div>
					<span>Hospitality</span>
			</div>
		</div>

		<div class="teaser-block__row">
			<div class="teaser-block__block" style="background:url('<?php echo get_template_directory_uri(); ?>/images/550.jpg');">
				<div class="tbb-label">
				</div>
				<span>Residential</span>

			</div>
			<div class="teaser-block__block" style="background:url('<?php echo get_template_directory_uri(); ?>/images/550.jpg');">
				<div class="tbb-label">
				</div>
				<span>Recreational</span>
			</div>
		</div>
	</div>
</section>


<?php //include_once('loops/scroll-up.php');?>

<?php get_footer(); ?>
