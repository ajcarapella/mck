/**!
 * Custom JS
 */

(function ($) {

	'use strict';

	$(document).ready(function() {

		// Comments

		$('.commentlist li').addClass('card');
		$('.comment-reply-link').addClass('btn btn-secondary');

		// Forms

		$('select, input[type=text], input[type=email], input[type=password], textarea').addClass('form-control');
		$('input[type=submit]').addClass('btn btn-primary');

		// Pagination fix for ellipsis

		$('.pagination .dots').addClass('page-link').parent().addClass('disabled');

		// Shrink navbar on scroll
		
	 	$(window).scroll(function() {
		if ($(document).scrollTop() > 150) {
			$('.navbar').addClass('shrink');
		}
		else {
			$('.navbar').removeClass('shrink'); }
		});

		//Smooth Scroll to Top
		$('#scroll-top-image').click(function () {
	        $('body,html').animate({
	            scrollTop: 0
	        }, 800);
	        return false;
	    });
		
		
		//Toggle Client List Accordion on Devices
		
		$('.client-list__row').click(function () {
			 $(this).toggleClass('expand-cl');
	    });
	    
	    
	    $('#management-loop h2').click(function () {
		    $('#management-loop').toggleClass('expand-ml');
	    })
		
		

	});

}(jQuery));
